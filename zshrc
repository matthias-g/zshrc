HISTFILE=~/.zsh/histfile
HISTSIZE=10000
SAVEHIST=10000
setopt hist_ignore_all_dups
setopt hist_ignore_space
setopt autocd
setopt extendedglob
bindkey -v

zstyle :compinstall filename '~/.zshrc'

autoload -Uz compinit
compinit
zstyle ':completion:*' menu select
setopt completealiases

autoload -U colors && colors

autoload -U promptinit && promptinit
export PS1="%n@%M:%~$ "
export PS1="%n@%{$fg[yellow]%}%m%{$reset_color%}:%~$ "

#setopt correctall

. ~/.zsh/special_keys

DIRSTACKFILE="$HOME/.zsh/dirs"
if [[ -f $DIRSTACKFILE ]] && [[ $#dirstack -eq 0 ]]; then
  dirstack=( ${(f)"$(< $DIRSTACKFILE)"} )
  [[ -d $dirstack[1] ]] && cd $dirstack[1]
fi
chpwd() {
  print -l $PWD ${(u)dirstack} >$DIRSTACKFILE
}
DIRSTACKSIZE=20
setopt autopushd pushdsilent pushdtohome
## Remove duplicate entries
setopt pushdignoredups
## This reverts the +/- operators.
setopt pushdminus

# rename files
# http://www.mfasold.net/blog/2008/11/moving-or-renaming-multiple-files/
autoload -U zmv
alias mmv='noglob zmv -W'

alias ls='ls --color=auto'

EDITOR=/usr/bin/vim
export EDITOR

cd ~
